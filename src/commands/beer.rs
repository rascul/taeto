use irc::client::prelude::{Config, Sender};
use log::error;
use reqwest::get;
use serde_json::Value;

use crate::command_message::CommandMessage;
use crate::Result;

#[derive(Clone, Debug)]
struct Beer {
	name: String,
	brewery: String,
	url: String,
}

impl Beer {
	fn milk() -> Self {
		Beer {
			name: "Milk".to_string(),
			brewery: "Borden".to_string(),
			url: "https://www.bordendairy.com/product/vitamin-d-whole-milk/".to_string(),
		}
	}

	async fn fetch(api_key: &str) -> Self {
		async fn do_fetch(api_key: &str) -> Result<Beer> {
			let url: String =
				["https://api.brewerydb.com/v2/beer/random/?key=", api_key, "&withBreweries=y"]
					.concat();

			let req = get(&url).await?;
			let json = req.json::<Value>().await?;

			let name = &json["data"]["name"].as_str().ok_or_else(|| "Unable to find beer name")?;

			let brewery = &json["data"]["breweries"][0]["name"]
				.as_str()
				.ok_or_else(|| "Unable to find brewery name")?;

			let url = [
				"https://brewerydbtemp.com/beer/",
				&json["data"]["id"].as_str().ok_or_else(|| "Unable to find beer id")?.to_string(),
			]
			.concat();

			Ok(Beer { name: name.to_string(), brewery: brewery.to_string(), url })
		}

		match do_fetch(api_key).await {
			Ok(r) => r,
			Err(e) => {
				error!("Fetching beer: {}", e);
				Self::milk()
			}
		}
	}
}

fn send_beer(sender: &Sender, cmd_msg: &CommandMessage, beer: &Beer) {
	match &cmd_msg.target {
		None => {
			let msg = [
				"\x01ACTION hands ",
				&cmd_msg.source,
				" a ",
				&beer.name,
				" from ",
				&beer.brewery,
				" (",
				&beer.url,
				")\x01",
			]
			.concat();

			let _ = sender.send_privmsg(&cmd_msg.channel, &msg);
		}
		Some(target) => {
			let msg = if target == "round" {
				[
					"\x01ACTION passes out a round of ",
					&beer.name,
					" from ",
					&beer.brewery,
					" (",
					&beer.url,
					")\x01",
				]
				.concat()
			} else {
				[
					"\x01ACTION hands ",
					&target,
					" a ",
					&beer.name,
					" from ",
					&beer.brewery,
					" (",
					&beer.url,
					")\x01",
				]
				.concat()
			};

			let _ = sender.send_privmsg(&cmd_msg.channel, msg);
		}
	};
}

pub async fn beer(config: &Config, sender: &Sender, cmd_msg: &CommandMessage) {
	if let Some(a) = config.options.get("beer_apikey") {
		send_beer(&sender, &cmd_msg, &Beer::fetch(&a).await)
	} else {
		send_beer(&sender, &cmd_msg, &Beer::milk())
	}
}
