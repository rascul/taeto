use futures::StreamExt;

use irc::client::data::config::Config;
use irc::client::prelude::{Client, Command};

use crate::command_message::CommandMessage;
use crate::commands;
use crate::result::Result;

pub async fn run() -> Result<()> {
	let config = Config::load("config.toml")?;
	let mut client = Client::from_config(config.clone()).await?;
	client.identify()?;

	let mut stream = client.stream()?;
	let sender = client.sender();

	while let Some(message) = stream.next().await.transpose()? {
		print!("{}", message);
		match message.command {
			Command::PRIVMSG(ref channel, ref msg) => {
				if let Some(prefix) = &message.prefix {
					if let Some(cmd_msg) = CommandMessage::parse(&prefix, &channel, &msg) {
						match cmd_msg.command.as_str() {
							"beer" => commands::beer(&config, &sender, &cmd_msg).await,
							_ => {}
						};
					}
				}
			}
			_ => (),
		}
	}

	Ok(())
}
