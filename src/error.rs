use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::io::Error as IoError;

use irc::error::Error as IrcError;
use log::SetLoggerError as LogError;
use reqwest::Error as ReqwestError;

#[derive(Debug)]
pub enum TaetoError {
	Io(IoError),
	Irc(IrcError),
	Log(LogError),
	Reqwest(ReqwestError),
	String(String),
}

impl From<IoError> for TaetoError {
	fn from(e: IoError) -> TaetoError {
		TaetoError::Io(e)
	}
}

impl From<IrcError> for TaetoError {
	fn from(e: IrcError) -> TaetoError {
		TaetoError::Irc(e)
	}
}

impl From<LogError> for TaetoError {
	fn from(e: LogError) -> TaetoError {
		TaetoError::Log(e)
	}
}

impl From<ReqwestError> for TaetoError {
	fn from(e: ReqwestError) -> TaetoError {
		TaetoError::Reqwest(e)
	}
}

impl From<&str> for TaetoError {
	fn from(e: &str) -> TaetoError {
		TaetoError::String(e.to_owned())
	}
}

impl Display for TaetoError {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match *self {
			TaetoError::Io(ref e) => Display::fmt(e, f),
			TaetoError::Irc(ref e) => Display::fmt(e, f),
			TaetoError::Log(ref e) => Display::fmt(e, f),
			TaetoError::Reqwest(ref e) => Display::fmt(e, f),
			TaetoError::String(ref e) => Display::fmt(e, f),
		}
	}
}
