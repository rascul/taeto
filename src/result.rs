use std::result::Result as StdResult;

use crate::error::TaetoError;

pub type Result<T> = StdResult<T, TaetoError>;
