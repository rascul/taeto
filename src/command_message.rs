use irc::client::prelude::Prefix;

#[derive(Clone, Debug)]
pub struct CommandMessage {
	pub message: String,
	pub command: String,
	pub channel: String,
	pub source: String,
	pub target: Option<String>,
}

impl CommandMessage {
	pub fn parse(prefix: &Prefix, target: &str, msg: &str) -> Option<Self> {
		let message = msg.to_owned();
		let parts: Vec<&str> = msg.split(" ").collect();

		if parts.is_empty() {
			return None;
		}

		let command = if parts[0].starts_with(".") {
			let (_, c) = parts[0].split_at(1);
			c.to_owned()
		} else {
			return None;
		};

		let channel = target.to_owned();

		let source = if let Prefix::Nickname(ref nick, ref _user, ref _host) = prefix {
			nick.to_owned()
		} else {
			return None;
		};

		let target: Option<String> =
			if let Some(target) = parts.get(1) { Some(target.to_string()) } else { None };

		Some(Self { message, command, channel, source, target })
	}
}
