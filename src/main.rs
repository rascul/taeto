mod command_message;
mod commands;
mod error;
mod result;
mod run;

use std::process::exit;

use log::error;

use result::Result;
use run::run;

#[tokio::main]
async fn main() -> Result<()> {
	rag::init()?;

	if let Err(e) = run().await {
		error!("{}", e);
		exit(1);
	}

	Ok(())
}
