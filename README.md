[![Pipeline Status](https://gitlab.com/rascul/taeto/badges/master/pipeline.svg)](https://gitlab.com/rascul/taeto/pipelines)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/rascul/taeto/blob/master/LICENSE)

taeto
=====

this is my irc bot

it is written in rust and you'll want to have the `cargo` command to build

to run it copy `config.toml.dist` to `config.toml`, edit `config.toml` to
your liking, then run `cargo run --release`

fields for the `config.toml` stuff are found at [https://rascul.gitlab.io/taeto/irc/client/data/config/struct.Config.html#fields](https://rascul.gitlab.io/taeto/irc/client/data/config/struct.Config.html#fields)

docs for all the dep crates is at [https://rascul.gitlab.io/taeto/taeto/index.html](https://rascul.gitlab.io/taeto/taeto/index.html)
